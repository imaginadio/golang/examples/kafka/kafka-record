package record

type Record struct {
	Name   string `json:"name,omitempty"`
	Random int    `json:"random,omitempty"`
}
